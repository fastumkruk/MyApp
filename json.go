package main

type Price struct {
	Date         string `json:"Date"`
	PreviousDate string `json:"PreviousDate"`
	PreviousURL  string `json:"PreviousURL"`
	Timestamp    string `json:"Timestamp"`
	Valute       struct {
		Amd struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"AMD"`
		Aud struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"AUD"`
		Azn struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"AZN"`
		Bgn struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"BGN"`
		Brl struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"BRL"`
		Byn struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"BYN"`
		Cad struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"CAD"`
		Chf struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"CHF"`
		Cny struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"CNY"`
		Czk struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"CZK"`
		Dkk struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"DKK"`
		Eur struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"EUR"`
		Gbp struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"GBP"`
		Hkd struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"HKD"`
		Huf struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"HUF"`
		Inr struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"INR"`
		Jpy struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"JPY"`
		Kgs struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"KGS"`
		Krw struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"KRW"`
		Kzt struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"KZT"`
		Mdl struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"MDL"`
		Nok struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"NOK"`
		Pln struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"PLN"`
		Ron struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"RON"`
		Sek struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"SEK"`
		Sgd struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"SGD"`
		Tjs struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"TJS"`
		Tmt struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"TMT"`
		Try struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"TRY"`
		Uah struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"UAH"`
		Usd struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"USD"`
		Uzs struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"UZS"`
		Xdr struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"XDR"`
		Zar struct {
			CharCode string  `json:"CharCode"`
			ID       string  `json:"ID"`
			Name     string  `json:"Name"`
			Nominal  int64   `json:"Nominal"`
			NumCode  string  `json:"NumCode"`
			Previous float64 `json:"Previous"`
			Value    float64 `json:"Value"`
		} `json:"ZAR"`
	} `json:"Valute"`
}
