package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"
)

const ShellToUse = "bash"

func main() {

	fmt.Println("Start...")
	////
	for {
		res, err := http.Get("https://www.cbr-xml-daily.ru/daily_json.js")
		if err != nil {
			panic(err.Error())
		}
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			panic(err.Error())
		}
		s, err := getPrice([]byte(body))
		err, mem, _ := Shellout(fmt.Sprintf("cat /proc/%d/smaps | grep Pss |  awk '{Total+=$2} END {print Total}'", os.Getpid()))
		if err != nil {
			log.Printf("error: %v\n", err)
		}
		err, cpuTime, _ := Shellout(fmt.Sprintf("ps -p %d -o etime=", os.Getpid()))
		if err != nil {
			log.Printf("error: %v\n", err)
		}
		Logs(fmt.Sprintf("\nUSD/RUB: %v\nProcess Time:%sMemory KB: %s", s.Valute.Usd.Value, cpuTime, mem))
		fmt.Println(fmt.Sprintf("\nUSD/RUB: %v\nProcess Time:%sMemory KB: %s", s.Valute.Usd.Value, cpuTime, mem))
		res.Body.Close()
		time.Sleep(30 * time.Second)
	}

}

func getPrice(body []byte) (*Price, error) {
	var s = new(Price)
	err := json.Unmarshal(body, &s)
	if err != nil {
		fmt.Println(err)
	}
	return s, err
}

func Logs(text string) {
	f, err := os.OpenFile("logs.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println(text)
}

func Shellout(command string) (error, string, string) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command(ShellToUse, "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return err, stdout.String(), stderr.String()
}
