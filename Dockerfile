FROM golang:latest
RUN mkdir /MyApp 
ADD . /MyApp/ 
WORKDIR /MyApp 
RUN go build -o main . 
ENTRYPOINT /MyApp/main
